package com.mitocode.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mitocode.model.Persona;
import com.mitocode.service.IPersonaService;

@RestController
@RequestMapping("/personas")
public class PersonaController {
	
	@Autowired
	private IPersonaService service;
	
	@GetMapping(value= "/listar", produces= MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Persona>> listar(){	
		List<Persona> personas = new ArrayList<>();
		try {
		personas = service.listar();
		}
		catch(Exception e)
		{
			return new ResponseEntity<List<Persona>>(personas,HttpStatus.INTERNAL_SERVER_ERROR);	
		}
		return new ResponseEntity<List<Persona>>(personas, HttpStatus.OK);	
		
	}
	
	@PostMapping(value="/registrar",consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Persona> registrar(@RequestBody Persona persona) {
		Persona per = new Persona();
		try {
			per = service.registrar(persona);
		}
		catch(Exception e)
		{
			return new ResponseEntity<Persona>(per,HttpStatus.INTERNAL_SERVER_ERROR);	
		}
		return new ResponseEntity<Persona>(per,HttpStatus.OK);
	}
		

}
