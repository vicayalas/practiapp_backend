package com.mitocode.service;

import java.util.List;

import com.mitocode.model.DetalleVenta;


public interface IDetalleVentaService {
	DetalleVenta registrar(DetalleVenta detalleVenta);

	List<DetalleVenta> listar();
}
