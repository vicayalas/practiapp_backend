package com.mitocode.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.mitocode.dao.IDetalleVentaDAO;
import com.mitocode.model.DetalleVenta;
import com.mitocode.service.IDetalleVentaService;

@Service
public class DetalleVentaServiceImpl implements IDetalleVentaService {
	
	private IDetalleVentaDAO dao;
	

	@Override
	public DetalleVenta registrar(DetalleVenta detalleVenta) {
		return dao.save(detalleVenta);
	}


	@Override
	public List<DetalleVenta> listar() {
		// TODO Auto-generated method stub
		return dao.findAll();
	}

}
